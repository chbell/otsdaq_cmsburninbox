#ifndef _ots_FECMSBurninBoxInterface_h_
#define _ots_FECMSBurninBoxInterface_h_

#include "otsdaq/FECore/FEVInterface.h"
#include "otsdaq/NetworkUtilities/TCPClient.h"
#include "otsdaq/NetworkUtilities/TCPPublishServer.h"
#include "BurninBox/BurninBoxUtils/BurninBoxController.h"

#include <atomic>
#include <mutex>

namespace ots
{
class FECMSBurninBoxInterface : public FEVInterface, public TCPClient
{

public:
	FECMSBurninBoxInterface     (const std::string& interfaceUID, const ConfigurationTree& theXDAQContextConfigTree, const std::string& interfaceConfigurationPath);
	virtual ~FECMSBurninBoxInterface(void);

	void configure        (void);
	void halt             (void);
	void pause            (void);
	void resume           (void);
	void start            (std::string runNumber="");
	void stop             (void);
	bool running          (void);

	void universalRead	  (char *address, char *returnValue) override {;}
	void universalWrite	  (char *address, char *writeValue) override {;}

	//MACROS
	void stateMachine(__ARGS__);
	void setTemperature(__ARGS__);
	void waitForTarget(__ARGS__);
	void waitAtTarget(__ARGS__);


private:
	std::string sendCommand(std::string buffer);

	//TCPClient         runningClient_;
	TCPPublishServer  dataPublisher_;
	std::atomic<bool> sendingCommand_;
	std::mutex        networkMutex_; // to make receiver socket thread safe

	bool verbose_;
	//BurninBoxController theBurninBoxController_;
};

}

#endif
