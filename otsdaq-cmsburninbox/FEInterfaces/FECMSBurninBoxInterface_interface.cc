#include "BurninBox/BurninBoxUtils/BurninBoxConfiguration.h"
#include "BurninBox/BurninBoxUtils/SafetyRangeDefinition.h"
#include "BurninBox/BurninBoxUtils/BurninBoxConfiguration.h"
#include "BurninBox/BurninBoxUtils/BurninBoxStatus.h"
#include "otsdaq-cmsburninbox/FEInterfaces/FECMSBurninBoxInterface.h"
#include "otsdaq/Macros/CoutMacros.h"
#include "otsdaq/Macros/InterfacePluginMacros.h"

//#include <stdio.h>
//#include <stdlib.h>
#include <iomanip>
#include <iostream>
//#include <fstream>
#include <sstream>
#include <string>
//#include <cstring> //for memcpy
//#include <set>

using namespace ots;

// size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream) {
//    std::string data((const char*) ptr, (size_t) size * nmemb);
//    *((std::stringstream*) stream) << data << std::endl;
//    return size * nmemb;
//}

//========================================================================================================================
ots::FECMSBurninBoxInterface::FECMSBurninBoxInterface(const std::string&       interfaceUID,
                                                      const ConfigurationTree& theXDAQContextConfigTree,
                                                      const std::string&       interfaceConfigurationPath)
    : FEVInterface(interfaceUID, theXDAQContextConfigTree, interfaceConfigurationPath)
    , TCPClient(theXDAQContextConfigTree.getNode(interfaceConfigurationPath)
                    .getNode("InterfaceIPAddress")
                    .getValue<std::string>(),
                theXDAQContextConfigTree.getNode(interfaceConfigurationPath)
                    .getNode("InterfacePort")
                    .getValue<unsigned int>())
    // , runningClient_(theXDAQContextConfigTree.getNode(interfaceConfigurationPath)
    //                 .getNode("InterfaceIPAddress")
    //                 .getValue<std::string>(),
    //             theXDAQContextConfigTree.getNode(interfaceConfigurationPath)
    //                 .getNode("InterfacePort")
    //                 .getValue<unsigned int>())
    , dataPublisher_(theXDAQContextConfigTree.getNode(interfaceConfigurationPath)
                         .getNode("DataPublisherPort")
                         .getValue<unsigned int>())
    , sendingCommand_(false)
    , verbose_(false)
{
    // MACROS REGISTRATION
    ////////////////////////////////////////////////////////////////////////////////////
    FEVInterface::registerFEMacroFunction(
        "StateMachine",                                                                             // feMacroName
        static_cast<FEVInterface::frontEndMacroFunction_t>(&FECMSBurninBoxInterface::stateMachine), // feMacroFunction
        std::vector<std::string>{"Transition"}, // START,STOP"},             // namesOfInputArgs
        std::vector<std::string>{},             // namesOfOutputArgs
        1);                                     // requiredUserPermissions

    ////////////////////////////////////////////////////////////////////////////////////
    FEVInterface::registerFEMacroFunction(
        "SetTemperature",                                                                             // feMacroName
        static_cast<FEVInterface::frontEndMacroFunction_t>(&FECMSBurninBoxInterface::setTemperature), // feMacroFunction
        std::vector<std::string>{"Temperature"}, //"},             // namesOfInputArgs
        std::vector<std::string>{},              // namesOfOutputArgs
        1);                                      // requiredUserPermissions

    ////////////////////////////////////////////////////////////////////////////////////
    FEVInterface::registerFEMacroFunction(
        "WaitForTarget",                                                                             // feMacroName
        static_cast<FEVInterface::frontEndMacroFunction_t>(&FECMSBurninBoxInterface::waitForTarget), // feMacroFunction
        std::vector<std::string>{}, //"},        // namesOfInputArgs
        std::vector<std::string>{},              // namesOfOutputArgs
        1);                                      // requiredUserPermissions

    ////////////////////////////////////////////////////////////////////////////////////
    FEVInterface::registerFEMacroFunction(
        "WaitAtTarget",                                                                             // feMacroName
        static_cast<FEVInterface::frontEndMacroFunction_t>(&FECMSBurninBoxInterface::waitAtTarget), // feMacroFunction
        std::vector<std::string>{"WaitTimeAtTarget"}, //"},        // namesOfInputArgs
        std::vector<std::string>{},              // namesOfOutputArgs
        1);                                      // requiredUserPermissions

    ////////////////////////////////////////////////////////////////////////////////////
    universalAddressSize_ = 8;
    universalDataSize_    = 8;
    dataPublisher_.startAccept();
}

//========================================================================================================================
ots::FECMSBurninBoxInterface::~FECMSBurninBoxInterface(void) {}

//========================================================================================================================
void ots::FECMSBurninBoxInterface::configure(void)
{
    __FE_COUT__ << "\tConfigure" << std::endl;

    BurninBoxConfiguration theConfiguration; 

    unsigned numberOfConfiguredTemperatures = 0;
    for(auto& temperature: getSelfNode().getNode("/LinkToBurninBoxCycleTable").getChildren())
    {
        if(temperature.second.getNode("InUse").getValue<std::string>() == "No") continue;
        std::cout << "Column: " << temperature.first << " -> " << temperature.second.getNode("Temperature").getValue<float>() << std::endl;
        if(temperature.second.getNode("Temperature").getValue<float>() < SafetyRangeDefinition::minTemperatureValue ||
           temperature.second.getNode("Temperature").getValue<float>() > SafetyRangeDefinition::maxTemperatureValue)
        {
            __CFG_SS__ << "TargetTemperature is not in the correct range [" +
                              std::to_string(SafetyRangeDefinition::minTemperatureValue) + "," +
                              std::to_string(SafetyRangeDefinition::maxTemperatureValue) + "]";
            __CFG_SS_THROW__;
        }
        theConfiguration.setTemperature(
            temperature.second.getNode("TargetID").getValue<std::string>(),
            temperature.second.getNode("Temperature").getValue<float>(),
            temperature.second.getNode("TimeAtTarget").getValue<unsigned>(),
            temperature.second.getNode("Sequence").getValue<unsigned>()
            );
        ++numberOfConfiguredTemperatures;
    }

    std::cout << "NEW JSON: " << theConfiguration.convertToJson() << std::endl;
    if(numberOfConfiguredTemperatures < 2)
    {
        __CFG_SS__ << "The configuration must have at least 2 temperatures. The target and the stop temperature.";
        __CFG_SS_THROW__;
    }

    std::string command = "CONFIGURE:"+theConfiguration.convertToJson();

    try
    {
        TCPClient::connect(30, 1000); // Tries to connect 30 times each second for 30 seconds and then gives up!
    }
    catch(const std::runtime_error& e)
    {
        __CFG_SS__ << e.what();
        __CFG_SS_THROW__;
    }

    std::string returnMessage = sendCommand(command);

    if(returnMessage.find("ERROR:") == 0)
    {
        __CFG_SS__ << returnMessage;
        __CFG_SS_THROW__;
    }
    __FE_COUT__ << "ConfigureDone!" << std::endl;
}

//========================================================================================================================
void ots::FECMSBurninBoxInterface::start(std::string runNumber)
{
    __FE_COUT__ << "\tStart" << std::endl;

	// //Start sequence
	// //	Start priority
	// //	

	// //Run means
	// //	burn in box gets to temperature
	// //	then enter running
	// //		do some calibration at that temperature
	// //		end running
	// // 

	// if(getIterationIndex == 0 &&
	// 	getNode("GoToTargetTemperature").getValue<bool>())
	// {
	// 	float temperature = getNode("TargetTemperature").getValue<float>();
	// 	__FE_COUT__ << "Go to temperature " << temperature << __E__;

	// 	float currentTemperature = readTemperature();
	// 	if(currentTemperature < temperature)
	// 		VStateMachine::indicateSubIterationWork();
	// }	


    sendCommand("START:{RunNumber:" + runNumber + "}");
}

//========================================================================================================================
void ots::FECMSBurninBoxInterface::stop(void)
{
    __FE_COUT__ << "\tStop" << std::endl;
    std::string message = sendCommand("STOP");
    if(message == "Stopping")
    {
        std::string statusBuffer = sendCommand("STATUS?");
        dataPublisher_.broadcastPacket(statusBuffer);
        VStateMachine::indicateSubIterationWork();
    }
    else if(message == "StopError")
    {
        __CFG_SS__ << "Burnin box had an error while stopping: " << message;
        __CFG_SS_THROW__;
    }
    //else everything is fine
}

//========================================================================================================================
bool ots::FECMSBurninBoxInterface::running(void)
{
    __FE_COUT__ << "Asking for status." << std::endl;
    std::string statusBuffer = sendCommand("STATUS?");
    dataPublisher_.broadcastPacket(statusBuffer);
    //__FE_COUT__ << "Returned status: " << statusBuffer << std::endl;
    //"Status buffer: " << statusBuffer << std::endl;
    sleep(5);
    return WorkLoop::continueWorkLoop_; // otherwise it stops!!!!!
}

//========================================================================================================================
void ots::FECMSBurninBoxInterface::halt(void)
{
    __FE_COUT__ << "\tHalt" << std::endl;
    std::string message = sendCommand("HALT");
    __FE_COUT__ << "\tMessage:" << message << std::endl;

    if(message == "Stopping")
    {
        std::string statusBuffer = sendCommand("STATUS?");
        dataPublisher_.broadcastPacket(statusBuffer);
        VStateMachine::indicateSubIterationWork();
    }
    else if(message == "StopError")
    {
        __CFG_SS__ << "Burnin box had an error while stopping: " << message;
        __CFG_SS_THROW__;
    }
}

//========================================================================================================================
void ots::FECMSBurninBoxInterface::pause(void)
{
    __FE_COUT__ << "\tPause" << std::endl;
    sendCommand("PAUSE");
}

//========================================================================================================================
void ots::FECMSBurninBoxInterface::resume(void)
{
    __FE_COUT__ << "\tResume" << std::endl;
    sendCommand("RESUME");
}

//========================================================================================================================
std::string ots::FECMSBurninBoxInterface::sendCommand(std::string buffer)
{
    // while(sendingCommand_)
    // {
    //     std::this_thread::sleep_for (std::chrono::milliseconds(200));//Donothing
    //     __FE_COUT__ << "waiting to send message" << std::endl;
    // }
    std::lock_guard<std::mutex> lock(networkMutex_);

//    sendingCommand_ = true;
    std::string readBuffer = sendAndReceivePacket(buffer);
//    sendingCommand_ = false;
    __FE_COUT__ << "Received message: " << readBuffer << std::endl;
    return readBuffer;
}

//========================================================================================================================
void FECMSBurninBoxInterface::stateMachine(__ARGS__)
{
    __FE_COUT__ << "# of input args = " << argsIn.size() << __E__;
    __FE_COUT__ << "# of output args = " << argsOut.size() << __E__;
    for(auto& argIn: argsIn)
        __FE_COUT__ << argIn.first << ": " << argIn.second << __E__;

    std::string transition = __GET_ARG_IN__("Transition", std::string);
    if(transition == "START")
    {
        // RUN NUMBER!!!!!!!
        start("0");
    }
    else if(transition == "STOP")
    {
        stop();
    }
    else
    {
        __FE_SS__ << "Cannot recognize state machine command " << transition << __E__;
        __FE_SS_THROW__;
    }
}

//========================================================================================================================
void FECMSBurninBoxInterface::setTemperature(__ARGS__)
{
    __FE_COUT__ << "Begin Macro SETTEMPERATURE!!!!" << __E__;
    __FE_COUT__ << "Begin Macro SETTEMPERATURE!!!!" << __E__;
    __FE_COUT__ << "Begin Macro SETTEMPERATURE!!!!" << __E__;
    __FE_COUT__ << "Begin Macro SETTEMPERATURE!!!!" << __E__;
    __FE_COUT__ << "Begin Macro SETTEMPERATURE!!!!" << __E__;
    __FE_COUT__ << "# of input args = " << argsIn.size() << __E__;
    __FE_COUT__ << "# of output args = " << argsOut.size() << __E__;
    for(auto& argIn: argsIn)
        __FE_COUT__ << argIn.first << ": " << argIn.second << __E__;

    std::string temperature = __GET_ARG_IN__("Temperature", std::string);
    // SEND SET TEMPERATURE COMMAND TO BURNIN BOX!
    //std::this_thread::sleep_for (std::chrono::seconds(1));
    sendCommand("SET_TARGET_TEMPERATURE:{Temperature:" + temperature + "}");
    std::this_thread::sleep_for (std::chrono::seconds(10));
    __FE_COUT__ << "Setting temperature: " << temperature << __E__;
    __FE_COUT__ << "Done Macro SETTEMPERATURE!!!!" << __E__;
    __FE_COUT__ << "Done Macro SETTEMPERATURE!!!!" << __E__;
    __FE_COUT__ << "Done Macro SETTEMPERATURE!!!!" << __E__;
    __FE_COUT__ << "Done Macro SETTEMPERATURE!!!!" << __E__;
    __FE_COUT__ << "Done Macro SETTEMPERATURE!!!!" << __E__;
}

//========================================================================================================================
void FECMSBurninBoxInterface::waitForTarget(__ARGS__)
{
    __FE_COUT__ << "Begin Macro WAIT_FOR_TARGET!!!!" << __E__;
    __FE_COUT__ << "Begin Macro WAIT_FOR_TARGET!!!!" << __E__;
    __FE_COUT__ << "Begin Macro WAIT_FOR_TARGET!!!!" << __E__;
    __FE_COUT__ << "Begin Macro WAIT_FOR_TARGET!!!!" << __E__;
    __FE_COUT__ << "Begin Macro WAIT_FOR_TARGET!!!!" << __E__;
    while(true) //should it be an atomic?
    {
        std::string statusBuffer = sendCommand("STATUS?");
        __FE_COUT__ << "Waiting with status: " << statusBuffer << __E__;
        __FE_COUT__ << "Status: " << statusBuffer << __E__; 
        BurninBoxStatus burninBoxStatus;
        burninBoxStatus.convertFromJSON(statusBuffer);
        if(burninBoxStatus.getCycleStatus() == static_cast<int>(BurninBoxController::Cycle::CYCLE_TARGET)) break;
        std::this_thread::sleep_for (std::chrono::seconds(10));
    }
    __FE_COUT__ << "Done Macro WAIT_FOR_TARGET!!!!" << __E__;
    __FE_COUT__ << "Done Macro WAIT_FOR_TARGET!!!!" << __E__;
    __FE_COUT__ << "Done Macro WAIT_FOR_TARGET!!!!" << __E__;
    __FE_COUT__ << "Done Macro WAIT_FOR_TARGET!!!!" << __E__;
    __FE_COUT__ << "Done Macro WAIT_FOR_TARGET!!!!" << __E__;
}


//========================================================================================================================
void FECMSBurninBoxInterface::waitAtTarget(__ARGS__)
{
    __FE_COUT__ << "Begin Macro WAIT_AT_TARGET!!!!" << __E__;
    __FE_COUT__ << "# of input args = " << argsIn.size() << __E__;
    __FE_COUT__ << "# of output args = " << argsOut.size() << __E__;
    for(auto& argIn: argsIn)
        __FE_COUT__ << argIn.first << ": " << argIn.second << __E__;

    int waitAtTargetTime = stoi(__GET_ARG_IN__("WaitTimeAtTarget", std::string));
    int checkTime = 5; //seconds

    while(waitAtTargetTime>0)
    {
        __FE_COUT__ << "Waiting at target for other " << waitAtTargetTime << " seconds " << __E__; 
        std::this_thread::sleep_for (std::chrono::seconds(checkTime));
        waitAtTargetTime-=checkTime;
    }
    
   __FE_COUT__ << "Done Macro WAIT_AT_TARGET!!!!" << __E__;
    __FE_COUT__ << "Done Macro WAIT_AT_TARGET!!!!" << __E__;
}

DEFINE_OTS_INTERFACE(ots::FECMSBurninBoxInterface)
