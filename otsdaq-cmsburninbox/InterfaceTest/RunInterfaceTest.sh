export CONFIGURATION_TYPE=File
export SERVICE_DATA_PATH=${USER_DATA}/ServiceData
export TABLE_INFO_PATH=${USER_DATA}/TableInfo
export OTSDAQ_LIB=${MRB_BUILDDIR}/otsdaq/lib
export OTSDAQ_UTILITIES_LIB=${MRB_BUILDDIR}/otsdaq_utilities/lib
export XDAQ_CONFIGURATION_XML=otsConfiguration_CMake_Run
export XDAQ_CONFIGURATION_DATA_PATH=${USER_DATA}/XDAQConfigurations
${CETPKG_BUILD}/otsdaq_cmsburninbox/bin/CMSBurninBoxInterfaceTestMain $1
