#include "otsdaq/ConfigurationInterface/ConfigurationManager.h"
#include "otsdaq/TableCore/TableGroupKey.h"
#include "otsdaq/ConfigurationInterface/ConfigurationInterface.h"
//#include "otsdaq-core/ConfigurationPluginDataFormats/Configurations.h"
//#include "otsdaq-core/ConfigurationPluginDataFormats/FEConfiguration.h"

#include "otsdaq-cmsburninbox/FEInterfaces/FECMSBurninBoxInterface_interface.cc"

#include <fstream>
#include <iostream>

using namespace ots;

int main(int argc, char** argv)
{
    // Variables
    const unsigned int configurationKeyValue_ = 7;

	ConfigurationManager* theConfigurationManager_ = new ConfigurationManager();

	std::string  XDAQContextTableName_         = "XDAQContextTable";
	std::string  supervisorContextUID_         = "BurninBoxContext";
	std::string  supervisorApplicationUID_     = "BurninBoxSupervisor";
	std::string  interfaceUID_                 = "FEBurninBox0";
	std::string  supervisorConfigurationPath_  = "/" + supervisorContextUID_ + "/LinkToApplicationTable/" + supervisorApplicationUID_ + "/LinkToSupervisorTable";
	const ConfigurationTree theXDAQContextConfigTree_ = theConfigurationManager_->getNode(XDAQContextTableName_);

    std::string                                   configurationGroupName = "BurninBoxConfiguration";
    std::pair<std::string, TableGroupKey> theGroup(configurationGroupName,
                                                           TableGroupKey(configurationKeyValue_));
    ////////////////////////////////////////////////////////////////
    // INSERTED GLOBALLY IN THE CODE
    ////////////////////////////////////////////////////////////////
    //
    //  ConfigurationManager*   theConfigurationManager_ = new ConfigurationManager;
    //  FEWInterfacesManager    theFEWInterfacesManager_(theConfigurationManager_, supervisorInstance_);
    //
    //  theConfigurationManager_->setupFEWSupervisorConfiguration(theConfigurationKey_,supervisorInstance_);
    //  theFEWInterfacesManager_.configure();
    //
    ////////////////////////////////////////////////////////////////
    // Getting just the informations about the FEWInterface
    ////////////////////////////////////////////////////////////////

    theConfigurationManager_->loadTableGroup(theGroup.first, theGroup.second, true);
    FECMSBurninBoxInterface* theInterface_ =
        new FECMSBurninBoxInterface(interfaceUID_,
                                    theXDAQContextConfigTree_,
                                    supervisorConfigurationPath_ + "/LinkToFEInterfaceTable/" + interfaceUID_ +
                                        "/LinkToFETypeTable");

    // Test interface class methods here //
    std::cout << __PRETTY_FUNCTION__ << "Configuring..." << std::endl;
    theInterface_->configure();
    // theInterface_->start(std::string(argv[1]));
    // unsigned int second = 1000; // x1ms
    // unsigned int time   = 60 * 60 * second;
	// unsigned int counter = 0;
    // while(counter++ < time)
    // {
    //     theInterface_->running(); // There is a 1ms sleep inside the running
    //     std::cout << counter << std::endl;
    // }
    // theInterface_->stop();

    return 0;
}
