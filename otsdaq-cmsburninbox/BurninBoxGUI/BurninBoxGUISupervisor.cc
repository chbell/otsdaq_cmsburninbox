#include "otsdaq-cmsburninbox/BurninBoxGUI/BurninBoxGUISupervisor.h"


#include <iostream>
#include <fstream>
#include <chrono>
#include <ctime>    
#include <utility>

using namespace ots;

#undef __MF_SUBJECT__
#define __MF_SUBJECT__ "BurninBoxGUI"

/*! the XDAQ_INSTANTIATOR_IMPL(ns1::ns2::...) macro needs to be put into the
 * implementation file (.cc) of the XDAQ application */
xdaq::Application* BurninBoxGUISupervisor::instantiate(xdaq::ApplicationStub* stub)
{
	return new BurninBoxGUISupervisor(stub);
}

//==============================================================================
// new user gets a table mgr assigned
// user can fill any of the tables (fill from version or init empty), which becomes the
// active view for that table
BurninBoxGUISupervisor::BurninBoxGUISupervisor(xdaq::ApplicationStub *stub)
	: ConfigurationGUISupervisor  (stub)
	, moduleConfigurationFileName_(__ENV__("MODULE_NAMES_FILE"))
{
	__SUP_COUT__ << "Constructor started." << __E__;
	INIT_MF("." /*directory used is USER_DATA/LOG/.*/);
	__SUP_COUT__ << "Constructor complete." << __E__;
} // end constructor()

//==============================================================================
BurninBoxGUISupervisor::~BurninBoxGUISupervisor(void) {}


//==============================================================================
void BurninBoxGUISupervisor::request(const std::string &requestType,
									 cgicc::Cgicc &cgiIn,
									 HttpXmlDocument &xmlOut,
									 const WebUsers::RequestUserInfo &userInfo)
{
	if(requestType == "ModuleNameFile")  //################################################################################################################
	{
		try
		{

			//	save
			//	load
			std::string cmd = cgiIn("cmd");  // possible commands are
			__SUP_COUT__ << "cmd " << cmd << __E__;

			if(cmd == "save")
			{
				std::string configuration = CgiDataUtilities::postData(cgiIn, "configuration");
				//__SUP_COUT__ << "configuration " << configuration << __E__;

				std::ofstream configurationFile;
				configurationFile.open (moduleConfigurationFileName_.c_str());
	
				if(!configurationFile.is_open())
				{
					xmlOut.addTextElementToData("status", "Error: Can't open file " + moduleConfigurationFileName_ + ". Likely the directory doesn't exist or you don't have the permissions to write in it.");
				}
				else
				{
					configurationFile << configuration;
					configurationFile.close();


					auto now = std::chrono::system_clock::now();
					std::time_t nowTime = std::chrono::system_clock::to_time_t(now);
					char buffer[80];
					struct tm * timeInfo;
					timeInfo = localtime(&nowTime);
					strftime(buffer, sizeof(buffer), "%Y-%m-%d_%Hh%Mm%Ss", timeInfo);
					
					std::string extFileName   = moduleConfigurationFileName_.substr(moduleConfigurationFileName_.find('.'), moduleConfigurationFileName_.length()-moduleConfigurationFileName_.find('.'));
					std::string timedFileName = moduleConfigurationFileName_.substr(0, moduleConfigurationFileName_.find('.')) + "_" + std::string(buffer) + extFileName;

					//__SUP_COUT__ << extFileName << " " << timedFileName << std::endl;
					// verify proper format through read back
					HttpXmlDocument cfgXml;
					if(cfgXml.loadXmlDocument(moduleConfigurationFileName_))
					{
						// successfully loaded, re-save for formatting
						cfgXml.saveXmlDocument(moduleConfigurationFileName_);
						cfgXml.saveXmlDocument(timedFileName);
						xmlOut.addTextElementToData("status", "Success");  // success
					}
					else  // failed to load properly
					{
						xmlOut.addTextElementToData("status", "Error: Improper file format.");
					}
				}
			}
			else if(cmd == "load")
			{
				HttpXmlDocument cfgXml;
				if(cfgXml.loadXmlDocument(moduleConfigurationFileName_))
				{
					xmlOut.addTextElementToData("status", "Success");
					xmlOut.copyDataChildren(cfgXml);  // copy file to output xml
				}
				else
				{
					xmlOut.addTextElementToData("status", "Error. Failed to properly load file " + moduleConfigurationFileName_ + ". Make sure it exists.");
				}
			}
			else
				xmlOut.addTextElementToData("status", "Error: Unrecognized command " + cmd);
		}
		catch (const std::runtime_error &e)
		{
			__SS__ << "A fatal error occurred while handling the request '" << requestType
				<< ".' Error: " << e.what() << __E__;
			__COUT_ERR__ << "\n"
						<< ss.str();
			xmlOut.addTextElementToData("Error", ss.str());

			try
			{
				// always add version tracking bool
				xmlOut.addTextElementToData(
					"versionTracking",
					ConfigurationInterface::isVersionTrackingEnabled() ? "ON" : "OFF");
			}
			catch (...)
			{
				__COUT_ERR__ << "Error getting version tracking status!" << __E__;
			}
		}
		catch (...)
		{
			__SS__ << "An unknown fatal error occurred while handling the request '"
				<< requestType << ".'" << __E__;
			__COUT_ERR__ << "\n"
						<< ss.str();
			xmlOut.addTextElementToData("Error", ss.str());

			try
			{
				// always add version tracking bool
				xmlOut.addTextElementToData(
					"versionTracking",
					ConfigurationInterface::isVersionTrackingEnabled() ? "ON" : "OFF");
			}
			catch (...)
			{
				__COUT_ERR__ << "Error getting version tracking status!" << __E__;
			}
		}
	}
	else//If it is not a specific request for the Burnin then fall back on the ConfigurationGUISupervisor::request
	{
		ConfigurationGUISupervisor::request(requestType, cgiIn, xmlOut, userInfo);
	}
}


