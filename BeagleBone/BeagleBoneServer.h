#ifndef _ots_BeagleBoneServer_h_
#define _ots_BeagleBoneServer_h_

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BeagleBone/BeagleBoneUtils/TCPNetworkServer.h"
#else
#include "TCPNetworkServer.h"
#endif

#include <string>
#include "BeagleBoneConfiguration.h"
#include "BurninBoxController.h"

namespace ots
{

class BeagleBoneServer: public TCPNetworkServer
{
public:

	BeagleBoneServer(BurninBoxController& burningBoxController, int serverPort, int bufferSize = 0x10000);
	virtual ~BeagleBoneServer(void);

	std::string readMessage(const std::string& buffer);

protected:

	std::string getVariableValue(std::string variable, std::string buffer)
	{
		size_t begin = buffer.find(variable)+variable.size()+1;
		size_t end   = buffer.find(',', begin);
		if(end == std::string::npos)
			end = buffer.size()-1;
		return buffer.substr(begin,end-begin);
	}

	BurninBoxController&    theBurninBoxController_;
	BeagleBoneConfiguration theBeagleBoneConfiguration_;
	std::string             currentRun_= "0";
	bool                    running_ = false;
	bool                    paused_=false;

};
}

#endif
