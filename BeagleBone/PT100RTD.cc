#include "PT100RTD.h"

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>

using namespace ots;

//Emanuele Aucone
const float a = 0.00385;
const float gain = 400/39.0;	//10.2564

//===========================================================================
PT100RTD::PT100RTD(std::string name, std::string id, std::string status)
: Device           (name, id, status)
, TemperatureSensor(name, id, status)
, AnalogSensor     (name, id, status)
{;}

//===========================================================================
PT100RTD::~PT100RTD(void) {;}

//Emanuele Aucone
//===========================================================================
float PT100RTD::readTemperature(void)
{
	float temperature = 0;
	float Vr = AnalogSensor::readValue(sensorPath_) / gain; //Vr = V / gain;
	float R = Vr / 0.001; //R = Vr / I, I = 0.001
	temperature = -247.29 + (R * (2.3992 + R * (0.00063962 + (0.0000010241 * R)))); //Cubic fit across -100 - 600 degree C (Version of the Callendar Van Dusen equation)
	return temperature;
} 


