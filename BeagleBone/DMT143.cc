#include "DMT143.h"

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>

using namespace ots;

//Emanuele Aucone
//===========================================================================
DMT143::DMT143(std::string name, std::string id, std::string status)
: Device          (name, id, status)
, DewPointSensor  (name, id, status)
, AnalogSensor    (name, id, status)
, resistorValue_  (90)
, dewPointRange_  (100)
, currentRange_   (16)
, minimumCurrent_ (4)
, minimumDewPoint_(-70)
{;}

//===========================================================================
DMT143::~DMT143(void)
{;}

//Emanuele Aucone
//===========================================================================
float DMT143::readDewPoint(void)
{
	float current = 1000 * AnalogSensor::readValue(sensorPath_)/resistorValue_; // 4-20 mA
	//std::cout << "Current read from dew point: " << current << std::endl;
	return (current-minimumCurrent_)/currentRange_*dewPointRange_ + minimumDewPoint_;
}

