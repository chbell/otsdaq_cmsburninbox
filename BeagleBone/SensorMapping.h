#ifndef _ots_SensorMapping_h_
#define _ots_SensorMapping_h_

#include <string>
#include <vector>
#include <map>
#include <fstream>

namespace ots
{
class SensorMapping
{
public:
	SensorMapping (void);
	~SensorMapping(void){;}

	void        setSensor    (std::string name, std::string sensorID);
	void        deleteSensor (std::string name                      );
	std::string getSensor    (std::string name                      );
	void        printMapping (void                                  );

	std::vector<std::map<std::string, std::string>> getSensorList           (void);
	std::vector<std::map<std::string, std::string>> getTemperatureSensorList(void) const;
	std::vector<std::map<std::string, std::string>> getDewPointSensorList   (void) const;
	std::vector<std::map<std::string, std::string>> getRelayList            (void) const;

private:
	std::fstream sensorFile_;
	// [SensorCategory] vector<sensors>(Attribute,  Value)
	// example:
	// sensor_["TEMPERATURE"]  [0]             ["name"]   =    "RTD0";
	std::map<std::string, std::vector<std::map<std::string, std::string> > > sensors_;
};
}
#endif
