#include "HM2500.h"

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>

using namespace ots;

//Emanuele Aucone
const float R1 = 100;
const float R2 = 120;
const float R0 = 100;

HM2500::HM2500(std::string name, std::string id, std::string status)
: Device        (name, id, status)
, HumiditySensor(name, id, status)
, AnalogSensor  (name, id, status)

{
	std::string temperatureId = id.substr(0, id.find(','));
	std::string humidityId    = id.substr(id.find(',')+1);
	temperaturePath_          = "/sys/bus/iio/devices/iio:device0/" + temperatureId;
	humidityPath_             = "/sys/bus/iio/devices/iio:device0/" + humidityId;
}

//Emanuele Aucone
//===========================================================================
float HM2500::readHumidity(void)
{
	float Vout = AnalogSensor::readValue(sensorPath_) * 1000;
	return -1.92*(pow(10,-9))*pow(Vout,3) + 1.44*(pow(10,-5))*pow(Vout,2) + 3.4*(pow(10,-3))*Vout -12.4;
}

