#ifndef _ots_Device_h_
#define _ots_Device_h_

#include <iostream>
#include <string>

namespace ots
{
class Device
{
public:
	Device(void)
: name_  ("")
, id_    ("")
, status_(false)
{std::cout << "IF YOU CALL ME, THERE IS SOMETHING WRONG! YOU NEED TO INITIALIZE THE OTHER CONSTRUCTOR IN THE CLASS THAT INHERITS ME!" << std::endl; exit(0);}
	Device(std::string name, std::string id, std::string status)
	: name_ (name)
	, id_   (id)
	{
		if(status == "ON" || status == "on" || status == "On")
			status_ = true;
		else if(status == "OFF" || status == "off" || status == "Off" )
			status_ = false;
		else
			std::cout << "Can't recognize status: " << status << ". You can only use: ON, On, on, OFF, Off, off." << std::endl;
	}
	virtual ~Device(void){;}

	void        setName        (std::string name    ){name_     = name;}
	void        setId          (std::string id      ){id_       = id;}
	void        setStatus      (bool status         ){status_   = status;}
	std::string getName        (void                ){return name_;}
	std::string getSensorID    (void                ){return id_;}
	bool        isOn           (void                ){return status_;}

protected:
	std::string name_;
	std::string id_;
	bool        status_;

};
}
#endif
