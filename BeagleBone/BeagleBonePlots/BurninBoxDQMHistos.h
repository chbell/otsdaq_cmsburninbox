#ifndef _ots_BurninBoxDQMHistos_h_
#define _ots_BurninBoxDQMHistos_h_

#include "BeagleBoneStatus.h"
#include <string>
#include <map>

class TCanvas;
class TPad;
class TDirectory;
class TGraph;
class TMultiGraph;

namespace ots
{

class ConfigurationTree;
class BurninBoxDQMHistos
{

public:
	BurninBoxDQMHistos(void);
	//BurninBoxDQMHistos(std::string supervisorApplicationUID, std::string bufferUID, std::string processorUID);
	virtual ~BurninBoxDQMHistos(void);

	void     book        (TDirectory* myDirectory);
	void     bookPads    (TDirectory* myDirectory);

	void     clear       ();
	void     fill        (std::string& buffer);
	void     fillPads    (std::string& buffer);
	void     load        (std::string fileName);
	void     writeGraphs (void);


protected:
	TDirectory*   myDir_;
	TDirectory*   statusDir_;

	TCanvas*      cTemperaturePads_;
	TPad*         pad1_;
	TPad*         pad2_;
	TPad*         pad3_;
	TPad*         pad4_;

	TCanvas*      cRelaysPads_;
	TPad*         padR1_;
	TPad*         padR2_;
	TPad*         padR3_;
	TPad*         padR4_;
	TPad*         padR5_;

	TCanvas*      cAmbientTemperature_;
	TCanvas*      cPlateTemperature_;
	TCanvas*      cDewPoint_;
	TCanvas*      cHumidity_;
	TCanvas*      cLeftPeltier_;
	TCanvas*      cRightPeltier_;
	TCanvas*      cChiller_;
	TCanvas*      cWarmup_;
	TCanvas*      cDryAirFlux_;
	TCanvas*      cRelays_;
	TCanvas*      cTemperatureMonitor_;

	TGraph*       gAmbientTemperature_;
	TGraph*       gPlateTemperature_;
	TGraph*       gDewPoint_;
	TGraph*       gHumidity_;
	TGraph*       gLeftPeltier_;
	TGraph*       gRightPeltier_;
	TGraph*       gChiller_;
	TGraph*       gWarmup_;
	TGraph*       gDryAirFlux_;
	TMultiGraph*  mgAllTemperatures_;


private:
	BeagleBoneStatus BeagleBoneStatus_;


};

}

#endif
