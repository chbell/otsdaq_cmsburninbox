#ifndef _ots_DeviceController_h_
#define _ots_DeviceController_h_

#include <string>
#include <vector>
#include <map>

namespace ots
{
class TemperatureSensor;
class DewPointSensor;
class Relay;
class Device;

class DeviceController
{
public:
	DeviceController (void);
	~DeviceController(void);

	void                         setupDevices         (std::vector<std::map<std::string, std::string>> sensorList);
	std::vector<std::string>     getDeviceList        (void);
	Device* const                getDevice            (std::string name);
	Relay*  const                getRelay             (std::string name);
	TemperatureSensor* const     getTemperatureSensor (std::string name);
	DewPointSensor* const        getDewPointSensor    (std::string name);
	double                       readTemperatures     (void);
	void                         printTemperatures    (void);
	double                       readDewPoints        (void);
	void                         printDewPoints       (void);

	double refreshDewPoints();

private:
	/* void setSensor       (std::string name, DS18B20* sensor); */
	//      <device name, pointer to sensor>
	std::map<std::string, Device*>            devices_;
	std::map<std::string, TemperatureSensor*> activeTemperatureSensorList_;
	std::map<std::string, DewPointSensor*>    activeDewPointSensorList_;
	std::map<std::string, Relay*>             activeRelayList_;
};
}
#endif
