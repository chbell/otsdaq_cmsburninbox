/*
 * BeagleBoneConfiguration.h
 *
 *  Created on: Sep 13, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */

#ifndef _ots_BeagleBoneConfiguration_h_
#define _ots_BeagleBoneConfiguration_h_

#include "JsonConverter.h"
#include <string>
#include <sys/time.h>

namespace ots
{

class BeagleBoneConfiguration : public JsonConverter
{
public:
	BeagleBoneConfiguration();
	BeagleBoneConfiguration(std::string streamToIPAddress, int streamToPort, float setTemperature, float lowTolerance, float highTolerance, float stopTemperature);
	virtual ~BeagleBoneConfiguration();
	//Setters
	void                    setStreamToIPAddress      (std::string value);
	void                    setStreamToPort           (int value);
	void                    setSetTemperature         (float value);
	void                    setLowTolerance           (float value);
	void                    setHighTolerance          (float value);
	void                    setStopTemperature        (float value);
	//Getters
	std::string             getStreamToIPAddress      (void) const;
	int                     getStreamToPort           (void) const;
	float                   getSetTemperature         (void) const;
	float                   getLowTolerance           (void) const;
	float                   getHighTolerance          (void) const;
	float                   getStopTemperature        (void) const;
};

}
#endif /* _ots_BeagleBoneConfiguration_h_ */
