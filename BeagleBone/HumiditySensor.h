#ifndef _ots_HumiditySensor_h_
#define _ots_HumiditySensor_h_

#include <string>
#include <math.h>

#include "Device.h"
#include <tgmath.h>

namespace ots
{
class HumiditySensor : virtual public Device
{
public:
	HumiditySensor(std::string name, std::string id, std::string status)
: Device(name, id, status)
{;}
	~HumiditySensor(void){;}

	virtual float readHumidity(void) = 0;

	float calculateTemperature(float dewPoint)
	{
		float humidity = readHumidity();
		return 243.04 * ( ( (17.625*dewPoint)/(243.04+dewPoint) ) - log(humidity/100) ) / ( 17.625 + log(humidity/100) - ( (17.625*dewPoint)/(243.04+dewPoint) ) );
	}

	float calculateDewPoint(float temperature)
	{
		float humidity = readHumidity();
		return 243.04 * ( log(humidity/100) + ( (17.625*temperature)/(243.04+temperature) ) ) / ( 17.625 - log(humidity/100) - ( (17.625*temperature)/(243.04+temperature) ) );
	}

};
}
#endif
