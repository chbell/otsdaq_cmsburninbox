#ifndef _ots_DMT143_h_
#define _ots_DMT143_h_

#include "AnalogSensor.h"
#include "DewPointSensor.h"
#include <string>

namespace ots
{
class DMT143 : public DewPointSensor, public AnalogSensor
{
public:
	DMT143(std::string name, std::string id, std::string status);
	~DMT143(void);

	float readDewPoint(void);

private:
	//Emanuele Aucone
	const float resistorValue_;
	const float dewPointRange_;// 30 - (-70) C
	const float currentRange_ ;// 20 - 4 mA
	const float minimumCurrent_ ;// 4 mA
	const float minimumDewPoint_ ;//-70C

	//	std::string sensorPath_;
//	float dataValue_; //in Celsius
};
}
#endif
