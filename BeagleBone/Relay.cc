#include "Relay.h"

using namespace ots;

//===========================================================================
Relay::Relay(std::string name, std::string id, std::string status)
: Device    (name, id, status)
, sensorPath_   ("/sys/class/gpio/export")
, directionPath_("/sys/class/gpio/" + id + "/direction")
, valuePath_    ("/sys/class/gpio/" + id + "/value")
//, direction_    (0)
//, idNumber_ (strtof((id.substr(id.find("gpio"+1))).c_str(), NULL))
{
	std::fstream exportFile(sensorPath_.c_str(), std::fstream::out);
	exportFile << id.substr(id.find("gpio")+4);
	exportFile.close();

	std::fstream directionFile(directionPath_.c_str(), std::fstream::out);
	directionFile << "out";
	directionFile.close();
	off();
}

//===========================================================================
Relay::~Relay(void)
{
	off();
}
//===========================================================================
void Relay::on(void)
{
	writeValue("1");
}

//===========================================================================
void Relay::off(void)
{
	writeValue("0");
}

//===========================================================================
bool Relay::isOn(void)
{
	std::string value;
	std::ifstream relayFile(valuePath_.c_str());
	if (!relayFile.is_open())
	{
		std::cout << "Error opening file: " << valuePath_ << std::endl;
		return true;
	}
	relayFile >> value;
	relayFile.close();
	if(value == "1")
		return true;
	else
		return false;
}

//===========================================================================
bool Relay::isOff(void)
{
	return !isOn();
}

//===========================================================================
void Relay::writeValue(std::string value)
{
	std::ofstream relayFile(valuePath_.c_str());
	if (!relayFile.is_open())
	{
		std::cout << "Error opening file: " << valuePath_ << std::endl;
		return;
	}
	relayFile << value;
	relayFile.close();
}
