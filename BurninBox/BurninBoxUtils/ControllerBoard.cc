#include "ControllerBoard.h"
#include "NetworkControllerProtocol.h"

#include <iostream>
#include <sstream>
#include <thread>
#include <chrono>

// For serial communication for DESY
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

using namespace ots;

//===========================================================================
ControllerBoard::ControllerBoard(std::string protocol, std::string version, std::string IPAddress, int port)
{
    if (protocol == "Network")
    {
        theProtocol_ = new NetworkControllerProtocol(IPAddress, port);
        dynamic_cast<NetworkControllerProtocol *>(theProtocol_)->setReceiveTimeout(10, 0);
    }
    // else if(protocol == "BeagleBone")
    //    theProtocol_ = new BeagleBoneProtocol;
    logFile_.open("ControllerBoardLog.log");
}

//===========================================================================
ControllerBoard::~ControllerBoard(void) 
{ 
    delete theProtocol_; 
    logFile_.close();
}

//===========================================================================
void ControllerBoard::setChillerSetPoint(const Device &device, int setpoint)
{

    if (device.getControllerConnector() == "BurninController")
    {
        std::string command = "[60OUT_MODE_01 " + std::to_string(setpoint) + "]";
        std::string value = theProtocol_->readValue(command);
        if (value != "*")
        {
            std::stringstream error;
            error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "1]Cannot set the chiller setpoint using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: *";
            logFile_ << error.str() << std::endl;
            //Retransmit and then give up if not working
            value = theProtocol_->readValue(command);
            if (value != "*")
            {
                error.str("");
                error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "2]Cannot set the chiller setpoint using command " << command << ", make sure the chiller is turned on and connected!";
                error << " Received: " << value << " Expected: *";
                logFile_ << error.str() << std::endl;
                throw std::runtime_error(error.str());
            }
        }
    }
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        // Do nothing
        return;
    }
    else
    {
        throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be used as an attribute in ControllerConnector for the device named Chiller in the CHILLER section of the configuration xml!");
    }
    // std::string command;
    // if(device.getControllerConnector() == "BurninController")
    //     command = "[60OUT_MODE_01 " + std::to_string(setpoint) + "]";
    // else
    // {
    //     throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be connected to the chiller!");
    // }
    // std::string value = theProtocol_->readValue(command);
    // //std::cout << __PRETTY_FUNCTION__ << "Chiller returned: " << value << std::endl;
    // // Check return value...
    // if(value != "*")
    //     throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
}

//===========================================================================
void ControllerBoard::setChillerTemperature(const Device &device, float temperature)
{
    if (device.getControllerConnector() == "BurninController")
    {
        std::string command = "[60OUT_SP_00 " + std::to_string(temperature) + "]";
        std::string value = theProtocol_->readValue(command);
        if (value != "*")
        {
            std::stringstream error;
            error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "1]Cannot set the chiller temperature using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: *";
            logFile_ << error.str() << std::endl;
            //Retransmit and then give up if not working
            value = theProtocol_->readValue(command);
            if (value != "*")
            {
                error.str("");
                error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "2]Cannot set the chiller temperature using command " << command << ", make sure the chiller is turned on and connected!";
                error << " Received: " << value << " Expected: *";
                logFile_ << error.str() << std::endl;
                throw std::runtime_error(error.str());
            }
        }
    }
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        std::stringstream command;
        command << "{M00" << std::hex << int(temperature * 100) << std::dec << "\r\n";
        std::string value = useDESYChiller(device, command.str());
        //std::cout << value << std::endl;
        //       if(value != "*")
        //           throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
    }
    else
    {
        throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be used as an attribute in ControllerConnector for the device named Chiller in the CHILLER section of the configuration xml!");
    }
    // std::string command;
    // if(device.getControllerConnector() == "BurninController")
    //     command = "[60OUT_SP_00 " + std::to_string(temperature) + "]";
    // else
    // {
    //     throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be connected to the chiller!");
    // }
    // std::string value = theProtocol_->readValue(command);
    // //std::cout << __PRETTY_FUNCTION__ << "Chiller returned: " << value << std::endl;
    // // Check return value...
    // if(value != "*")
    //     throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
}

//===========================================================================
float ControllerBoard::readChillerTemperature(const Device &device)
{
    if (device.getControllerConnector() == "BurninController")
    {
        std::string command = "[61IN_SP_00]";
        std::string value = theProtocol_->readValue(command);
        try
        {
           return strtof(&value.at(0), nullptr);
        }
        catch(const std::exception& e)
        {
            std::stringstream error;
            error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "1]Cannot read the chiller temperature using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: float";
            logFile_ << error.str() << std::endl;
            //Retransmit and then give up if not working
            value = theProtocol_->readValue(command);
            try
            {
                return strtof(&value.at(0), nullptr);
            }
            catch(const std::exception& e)
            {
                error.str("");
                error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "2]Cannot read the chiller temperature using command " << command << ", make sure the chiller is turned on and connected!";
                error << " Received: " << value << " Expected: float";
                logFile_ << error.str() << std::endl;
                throw std::runtime_error(error.str());
            }
        }       
    }
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        std::stringstream command;
        command << "{M00****\r\n";
        std::string value = useDESYChiller(device, command.str());
        if (value.substr(0, 4) != "{S00")
        {
            throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
        }
        std::string retVal = "0x" + value.substr(4, 4);
        return std::stoul(retVal, nullptr, 16) / 100.;
    }
    else
    {
        throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be used as an attribute in ControllerConnector for the device named Chiller in the CHILLER section of the configuration xml!");
    }

    // std::string command;
    // if(device.getControllerConnector() == "BurninController")
    //     command = "[61IN_SP_00]";
    // else
    // {
    //     throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be connected to the chiller!");
    // }
    // std::string value = theProtocol_->readValue(command);
    // return strtof(&value.at(0), nullptr);
}

//===========================================================================
void ControllerBoard::turnOnChiller(const Device &device)
{
    if (device.getControllerConnector() == "BurninController")
    {
        std::string command = "[60OUT_MODE_05 1]";
        std::string value = theProtocol_->readValue(command);
        if (value != "*")
        {
            std::stringstream error;
            error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "1]Cannot turn on the chiller using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: *";
            logFile_ << error.str() << std::endl;
            //Retransmit and then give up if not working
            value = theProtocol_->readValue(command);
            if (value != "*")
            {
                error.str("");
                error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "2]Cannot turn on the chiller using command " << command << ", make sure the chiller is turned on and connected!";
                error << " Received: " << value << " Expected: *";
                logFile_ << error.str() << std::endl;
                throw std::runtime_error(error.str());
            }
        }
        // Wait 2 seconds before the chiller actually turns on
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        std::stringstream command;
        command << "{M140001\r\n";
        std::string value = useDESYChiller(device, command.str());
        //std::cout << value << std::endl;
        //        if(value != "*")
        //           throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
        // Wait 2 seconds before the chiller actually turns on
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
    else
    {
        throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be used as an attribute in ControllerConnector for the device named Chiller in the CHILLER section of the configuration xml!");
    }

    // std::cout << __PRETTY_FUNCTION__ << "Turning on chiller" << std::endl;
    //  std::string command;
    //  if(device.getControllerConnector() == "BurninController")
    //      command = "[60OUT_MODE_05 1]";
    //  else
    //  {
    //      throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be connected to the chiller!");
    //  }
    //  std::string value = theProtocol_->readValue(command);
    //  //std::cout << __PRETTY_FUNCTION__ << "Chiller returned: " << value << std::endl;
    //  // Check return value...
    //  if(value != "*")
    //      throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
    //  //Wait 2 seconds before the chiller actually turns on
    //  std::this_thread::sleep_for(std::chrono::seconds(2));
}

//===========================================================================
void ControllerBoard::turnOffChiller(const Device &device)
{
    return;
    if (device.getControllerConnector() == "BurninController")
    {
        std::string command = "[60OUT_MODE_05 0]";
        std::string value = theProtocol_->readValue(command);
        if (value != "*")
        {
            std::stringstream error;
            error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "1]Cannot turn off the chiller using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: *";
            logFile_ << error.str() << std::endl;
            //Retransmit and then give up if not working
            value = theProtocol_->readValue(command);
            if (value != "*")
            {
                error.str("");
                error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "2]Cannot turn off the chiller using command " << command << ", make sure the chiller is turned on and connected!";
                error << " Received: " << value << " Expected: *";
                logFile_ << error.str() << std::endl;
                throw std::runtime_error(error.str());
            }
        }
        // Wait 2 seconds before the chiller actually turns on
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        std::stringstream command;
        command << "{M140000\r\n";
        std::string value = useDESYChiller(device, command.str());
        //std::cout << value << std::endl;
        //       if(value != "*")
        //           throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
        // Wait 2 seconds before the chiller actually turns on
        std::this_thread::sleep_for(std::chrono::seconds(2));
    }
    else
    {
        throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be used as an attribute in ControllerConnector for the device named Chiller in the CHILLER section of the configuration xml!");
    }
    // std::cout << __PRETTY_FUNCTION__ << "Turning off chiller" << std::endl;
    //  std::string command;
    //  if(device.getControllerConnector() == "BurninController")
    //      command = "[60OUT_MODE_05 0]";
    //  else
    //  {
    //      throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be connected to the chiller!");
    //  }
    //  std::string value = theProtocol_->readValue(command);
    //  //std::cout << __PRETTY_FUNCTION__ << "Chiller returned: " << value << std::endl;
    //  // Check return value...
    //  if(value != "*")
    //      throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
    //  //Wait 2 seconds before the chiller actually turns off
    //  std::this_thread::sleep_for(std::chrono::seconds(2));
}

//===========================================================================
bool ControllerBoard::isChillerOn(const Device &device)
{
    if (device.getControllerConnector() == "BurninController")
    {
        std::string command = "[61IN_MODE_05]";
        std::string value = theProtocol_->readValue(command);
        if (value == "0")
            return false;
        else if (value == "1")
            return true;
        else
        {
            std::stringstream error;
            error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "1]Cannot check if the chiller is on using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: 0/1";
            logFile_ << error.str() << std::endl;
            //Retransmit and then give up if not working
            value = theProtocol_->readValue(command);
            if (value == "0")
                return false;
            else if (value == "1")
                return true;
            else
            {
                error.str("");
                error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "2]Cannot check if the chiller is on using command " << command << ", make sure the chiller is turned on and connected!";
                error << " Received: " << value << " Expected: 0/1";
                logFile_ << error.str() << std::endl;
                return true;//ASSUMING THAT THE CHILLER IS ON AND EVENTUALLY MUST BE TURNED OFF!
                //Removing exceptions because I don't want to stop everything if I get an error once!
                // std::stringstream error;
                // error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "Cannot set the chiller temperature using command [60OUT_SP_00], make sure the chiller is turned on and connected!";
                // throw std::runtime_error(error.str());           
            }
        }
    }
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        std::stringstream command;
        command << "{M14****\r\n";
        std::string value = useDESYChiller(device, command.str());
        if (value.substr(0, 4) != "{S14")
        {
            throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
        }
        if (value.substr(4, 4) == "0000")
            return false;
        else if (value.substr(4, 4) == "0001")
            return true;
        else
            throw std::runtime_error("Weird chiller response: " + value);
    }
    else
    {
        throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be used as an attribute in ControllerConnector for the device named Chiller in the CHILLER section of the configuration xml!");
    }
    // std::string command;
    // if(device.getControllerConnector() == "BurninController")
    //     command = "[61IN_MODE_05]";
    // else
    // {
    //     throw std::runtime_error("Device: " + device.getControllerConnector() + " cannot be connected to the chiller!");
    // }
    // std::string value = theProtocol_->readValue(command);
    // //std::cout << __PRETTY_FUNCTION__ << "Chiller returned: " << value << std::endl;
    // // Check return value...
    // if(value == "0")
    //     return false;
    // else if (value == "1")
    //     return true;
    // else
    //     throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
}

//===========================================================================
float ControllerBoard::readTemperature(const Device &device)
{
    std::string command;
    if (device.getControllerConnector() == "RTD1")
        command = "[21]";
    else if (device.getControllerConnector() == "RTD2")
        command = "[22]";
    else if (device.getControllerConnector() == "RTD3")
        command = "[23]";
    else if (device.getControllerConnector() == "RTD4")
        command = "[24]";
    else if (device.getControllerConnector() == "DTS1")
        command = "[4101]";
    else if (device.getControllerConnector() == "DTS2")
        command = "[4102]";
    else if (device.getControllerConnector() == "DTS3")
        command = "[4103]";
    else if (device.getControllerConnector() == "DTS4")
        command = "[4104]";
    else if (device.getControllerConnector() == "DTS5")
        command = "[4105]";
    else if (device.getControllerConnector() == "DTS6")
        command = "[4106]";
    else if (device.getControllerConnector() == "DTS7")
        command = "[4107]";
    else if (device.getControllerConnector() == "DTS8")
        command = "[4108]";
    else if (device.getControllerConnector() == "DTS9")
        command = "[4109]";
    else if (device.getControllerConnector() == "DTS10")
        command = "[4110]";
    else if (device.getControllerConnector() == "ChillerBath")
        command = "[61IN_PV_00]";
    else if (device.getControllerConnector() == "ChillerExternRTD")
        command = "[61IN_PV_02]";
    else if (device.getControllerConnector().find("/dev/tty") == 0)
    {
        std::stringstream command;
        command << "{M01****\r\n";
        std::string value = useDESYChiller(device, command.str());
        if (value.substr(0, 4) != "{S01")
        {
            throw std::runtime_error("Chiller reply is not the expected one, make sure the chiller is on and connected!");
        }
        std::string retVal = "0x" + value.substr(4, 4);
        return std::stoul(retVal, nullptr, 16) / 100.;
    }
    else
    {
        throw std::runtime_error("Device: " + device.getControllerConnector() + " is not a temperature device!");
    }

    //    std::cout << __PRETTY_FUNCTION__ << device.getControllerConnector() << " sending command: " << command << std::endl;
    std::string value = theProtocol_->readValue(command);
    try
    {
        return strtof(&value.at(0), nullptr);
    }
    catch(const std::exception& e)
    {
        std::stringstream error;
        error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "1]Cannot read temperature using command " << command << ", make sure the chiller is turned on and connected!";
        error << " Received: " << value << " Expected: float";
        logFile_ << error.str() << std::endl;
        //Retransmit and then give up if not working
        value = theProtocol_->readValue(command);
        try
        {
            return strtof(&value.at(0), nullptr);
        }
        catch(const std::exception& e)
        {
            error.str("");
            error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "2]Cannot read temperature using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: float";
            logFile_ << error.str() << std::endl;
            throw std::runtime_error(error.str());
        }
    }       
}

//===========================================================================
float ControllerBoard::readHumidity(const Device &device) { return 100; }

//===========================================================================
float ControllerBoard::readDewPoint(const Device &device)
{
    //    std::cout << __PRETTY_FUNCTION__ << "Reading device: " << device.getControllerConnector() << std::endl;
    std::string command;
    if (device.getControllerConnector() == "H&T1")
        command = "[30]";
    else if (device.getControllerConnector() == "H&T2")
        command = "[31]";
    else
    {
        std::cout << __PRETTY_FUNCTION__ << "Device: " << device.getControllerConnector()
                  << " is not in the list of readable devices! Crashing now...!" << std::endl;
        abort();
    }

    std::string value = theProtocol_->readValue(command);
    try
    {
        return strtof(&value.at(0), nullptr);
    }
    catch(const std::exception& e)
    {
        std::stringstream error;
        error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "1]Cannot read dew point using command " << command << ", make sure the chiller is turned on and connected!";
        error << " Received: " << value << " Expected: float";
        logFile_ << error.str() << std::endl;
        //Retransmit and then give up if not working
        value = theProtocol_->readValue(command);
        try
        {
            return strtof(&value.at(0), nullptr);
        }
        catch(const std::exception& e)
        {
            error.str("");
            error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "2]Cannot read dew point using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: float";
            logFile_ << error.str() << std::endl;
            throw std::runtime_error(error.str());
        }
    }       
}

//===========================================================================
void ControllerBoard::relayOn(const Device &device)
{
    // std::cout << __PRETTY_FUNCTION__ << "Turning on relay: " << device.getControllerConnector() << std::endl;
    // return;
    std::string command;
    if (device.getControllerConnector() == "RLY1")
        command = "[5011]";
    else if (device.getControllerConnector() == "RLY2")
        command = "[5021]";
    else if (device.getControllerConnector() == "RLY3")
        command = "[5031]";
    else if (device.getControllerConnector() == "RLY4")
        command = "[5041]";
    else
    {
        throw std::runtime_error("Device: " + device.getControllerConnector() + " is not a relay!");
    }
    std::string value = theProtocol_->readValue(command);
    if (value != "*")
    {
        std::stringstream error;
        error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "1]Cannot turn on relay using command " << command << ", make sure the chiller is turned on and connected!";
        error << " Received: " << value << " Expected: *";
        logFile_ << error.str() << std::endl;
        //Retransmit and then give up if not working
        value = theProtocol_->readValue(command);
        if (value != "*")
        {
            error.str("");
            error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "2]Cannot turn on relay using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: *";
            logFile_ << error.str() << std::endl;
            throw std::runtime_error(error.str());
        }
    }
}

//===========================================================================
void ControllerBoard::relayOff(const Device &device)
{
    // std::cout << __PRETTY_FUNCTION__ << "Turning off relay: " << device.getControllerConnector() << std::endl;
    // return;
    std::string command;
    if (device.getControllerConnector() == "RLY1")
        command = "[5010]";
    else if (device.getControllerConnector() == "RLY2")
        command = "[5020]";
    else if (device.getControllerConnector() == "RLY3")
        command = "[5030]";
    else if (device.getControllerConnector() == "RLY4")
        command = "[5040]";
    else
    {
        throw std::runtime_error("Device: " + device.getControllerConnector() + " is not a relay!");
    }
    std::string value = theProtocol_->readValue(command);
    if (value != "*")
    {
        std::stringstream error;
        error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "1]Cannot turn off relay using command " << command << ", make sure the chiller is turned on and connected!";
        error << " Received: " << value << " Expected: *";
        logFile_ << error.str() << std::endl;
        //Retransmit and then give up if not working
        value = theProtocol_->readValue(command);
        if (value != "*")
        {
            error.str("");
            error << __PRETTY_FUNCTION__ << " [" << __LINE__ << "]\t" << "2]Cannot turn off relay using command " << command << ", make sure the chiller is turned on and connected!";
            error << " Received: " << value << " Expected: *";
            logFile_ << error.str() << std::endl;
            throw std::runtime_error(error.str());
        }
    }
}

//===========================================================================
void ControllerBoard::writeChiller(const std::string &command) {}

//===========================================================================
std::string ControllerBoard::readChiller(const std::string &command) { return "ok"; }

//===========================================================================
std::string ControllerBoard::executeCommand(std::string command)
{
    // std::cout << __PRETTY_FUNCTION__ << "Executing command: " << command << std::endl;
    std::string value = theProtocol_->readValue(command);
    // std::cout << __PRETTY_FUNCTION__ << "Command returned: " << value << std::endl;
    return value;
}

//===========================================================================
std::string ControllerBoard::useDESYChiller(const Device &device, std::string command)
{
    std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Desy begin: " << device.getControllerConnector().c_str() << std::endl;
    // Open the serial port
    int fd = open(device.getControllerConnector().c_str(), O_RDWR | O_NOCTTY | O_NONBLOCK);
    if (fd == -1)
    {

        std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Error opening serial port" << std::endl;
        throw std::runtime_error("Error opening serial port " + device.getControllerConnector());
    }

    // Configure the serial port
    termios options;
    tcgetattr(fd, &options);
    cfsetispeed(&options, B9600);
    cfsetospeed(&options, B9600);
    //    options.c_cflag &= ~PARENB;
    //    options.c_cflag &= ~CSTOPB;
    //    options.c_cflag &= ~CSIZE;
    //    options.c_cflag |= CS8;
    //    options.c_cflag &= ~CRTSCTS;
    //    options.c_cflag |= CREAD | CLOCAL;
    //    options.c_iflag &= ~(IXON | IXOFF | IXANY);
    //    options.c_iflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    //    options.c_oflag &= ~OPOST;
    //    options.c_cc[VMIN] = 0;
    //    options.c_cc[VTIME] = 10;
    //    tcsetattr(fd, TCSANOW, &options);

    // Send a command to the device
    // std::string command = "{M00****\r\n";//THIS IS AREAL DESY CHILLER COMMAND
    //std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Command: " << command << " : " << command.length() << std::endl;
    write(fd, command.c_str(), command.length());
    std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Written" << std::endl;

    // Read the response from the device
    char buffer[1024];

    //    std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Reading" << std::endl;
    int bytes_read = 0;
    for (unsigned timeout = 0; timeout < 10 && bytes_read <= 0; timeout++)
    {
        usleep(200000);
        bytes_read = read(fd, buffer, sizeof(buffer) - 1);
        //	std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Buffer: " <<  buffer << " : " << bytes_read << std::endl;
    }

    //std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Buffer: " << buffer << std::endl;
    if (bytes_read == -1)
    {
        std::cout << __LINE__ << "]" << __PRETTY_FUNCTION__ << "Error opening serial port" << std::endl;
        throw std::runtime_error("Error reading from serial port " + device.getControllerConnector());
    }
    buffer[bytes_read] = '\0';

    // Print the response to the console
    //std::cout << "Response: " << buffer << std::endl;

    // Close the serial port
    close(fd);
    return buffer;
}
