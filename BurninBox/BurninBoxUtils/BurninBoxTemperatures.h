/*
 * BurninBoxTemperatures.h
 *
 *  Created on: Sep 13, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */

#ifndef _ots_BurninBoxTemperatures_h_
#define _ots_BurninBoxTemperatures_h_

#include <map>
#include <string>

namespace ots
{

  class BurninBoxTemperatures
  {
  public:
    struct Temperature
    {
      Temperature() { ; }
      Temperature(std::string _name, double _temperature, unsigned _time, unsigned _sequence=-1)
          : sequence(_sequence), name(_name), temperature(_temperature), time(_time)
      {
      }

      unsigned sequence;
      std::string name;
      double temperature;
      unsigned time;
    };

  public:
    BurninBoxTemperatures();
    virtual ~BurninBoxTemperatures();

    const std::string &getClassName(void) { return className_; }
    void clearTemperatures();

    // Setters
    virtual void setTemperature(std::string name, double temperature, unsigned time, unsigned sequence=-1);
    virtual void setTemperature(std::string name, const Temperature &temperature);

    // Getters
    Temperature getTemperature(std::string name) const;
    const std::map<std::string, Temperature> &getTemperatures() const { return temperatures_; };

    void printTemperatures();
    auto begin() { return temperatures_.begin(); }
    auto end() { return temperatures_.end(); }
    auto cbegin() const { return temperatures_.begin(); }
    auto cend() const { return temperatures_.end(); }
    auto begin() const { return temperatures_.begin(); }
    auto end() const { return temperatures_.end(); }

  protected:
    std::map<std::string, Temperature> temperatures_;
    const std::string className_;
  };

} // namespace ots
#endif /* _ots_BurninBoxTemperatures_h_ */
