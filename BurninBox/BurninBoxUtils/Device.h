#ifndef _ots_Device_h_
#define _ots_Device_h_

#include <map>
#include <string>
#include <stdexcept>


namespace ots
{
typedef std::map<std::string, std::string> ConfigurationAttributes;

class Device
{
  public:
    Device(void);
    Device(std::string name, std::string connector, std::string displayName, std::string comment, std::string status);
    Device(const ConfigurationAttributes& attributes);
    virtual ~Device(void);

    void setAttributes(std::string name, std::string connector, std::string displayName, std::string comment, std::string status);
    void setAttributes(const ConfigurationAttributes& attributes);

    std::string getName(void) const;
    std::string getControllerConnector(void) const;
    std::string getDisplayName(void) const;
    std::string getComment(void) const;
    bool        isOn(void) const;
    static bool isOn(std::string status)
    {
        if(status == "ON" || status == "on" || status == "On")
            return true;
        else if(status == "OFF" || status == "off" || status == "Off")
            return false;
        else
            throw(std::runtime_error("Can't recognize status: " + status +
                                     ". You can only use: ON, On, on, OFF, Off, off."));
    }

  protected:
    std::string name_;
    std::string controllerConnector_;
    std::string displayName_;
    std::string comment_;
    bool        status_;
};
} // namespace ots
#endif
