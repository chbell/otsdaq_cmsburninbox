/*
 * JsonConverter.h
 *
 *  Created on: Sep 12, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */
#ifndef _ots_JsonConverter_h_
#define _ots_JsonConverter_h_

#include <map>
#include <string>

namespace ots
{

class JsonConverter
{
public:
	JsonConverter(std::string name);
	virtual ~JsonConverter();

	const std::string&          getClassName    (void);
	void                        printVariables  (void);
	void                        printToFile     (std::ofstream& logfile);
	void                        printToScreen   (void);
	virtual const std::string&  convertToJSON   (void);
	virtual void                convertFromJSON (const std::string& json);

protected:
	void getVariableValue(const std::string& variable, std::string& variableValue, const std::string& json);

	std::string                        className_;
	std::map<std::string, std::string> variables_;
	std::string                        json_;
};

}
#endif /* _ots_JsonConverter_h_ */
