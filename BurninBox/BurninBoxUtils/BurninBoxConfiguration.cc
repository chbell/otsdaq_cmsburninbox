/*
 * BurninBoxConfiguration.cc
 *
 *  Created on: Sep 13, 2018
 *      Author: otsdaq
 */

#ifndef BEAGLEBONE
#include "BurninBox/BurninBoxUtils/BurninBoxConfiguration.h"
#else
#include "BurninBoxConfiguration.h"
#endif

#include <iostream>

using namespace ots;

#include <nlohmann/json.hpp>

using json = nlohmann::json;

namespace ots
{
void to_json(nlohmann::json& j, const BurninBoxTemperatures::Temperature& t)
{
    j = nlohmann::json{{"name", t.name}, {"temperature", t.temperature}, {"time", t.time}, {"sequence", t.sequence}};
}

void from_json(const nlohmann::json& j, BurninBoxTemperatures::Temperature& t)
{
    j.at("name").get_to(t.name);
    j.at("temperature").get_to(t.temperature);
    j.at("time").get_to(t.time);
    j.at("sequence").get_to(t.sequence);
}

} // namespace ots

//========================================================================================================================
BurninBoxConfiguration::BurninBoxConfiguration() : className_ ("BurninBoxConfiguration"){}

//========================================================================================================================
BurninBoxConfiguration::~BurninBoxConfiguration() {}

// //========================================================================================================================
// std::string BurninBoxTemperatures::convertToJson(void)
// {
//     json jsonOut;
//     jsonOut[className_] = {json(temperatures_)};

//     return jsonOut.dump();
// }

//========================================================================================================================
// void BurninBoxTemperatures::convertFromJson(std::string jsonString)
// {
//     temperatures_.clear();
//     json jsonIn = json::parse(jsonString);

//     auto temperaturesJson = jsonIn.find(className_);
//     if(temperaturesJson == jsonIn.end())
//     {
//         throw std::runtime_error("The json I am trying to parse (" + jsonString + ") is not of class " + className_);
//     }
//     else
//     {
//         for(auto& temperature: temperaturesJson.value()[0]) // There is only 1 object associated to className_
//         {
//             temperatures_[temperature["name"]] = temperature.get<Temperature>();
//         }
//     }
// }

//========================================================================================================================
void BurninBoxConfiguration::setTemperature(std::string name, double temperature, unsigned time, unsigned sequence)
{
    temperatures_.setTemperature(name, temperature, time, sequence);
}

//========================================================================================================================
std::string BurninBoxConfiguration::convertToJson(void)
{
    json jsonOut;
    jsonOut[className_][temperatures_.getClassName()] = {json(temperatures_.getTemperatures())};
    std::cout << jsonOut << std::endl;

    return jsonOut.dump();
}

//========================================================================================================================
void BurninBoxConfiguration::convertFromJson(std::string jsonString)
{
    json jsonIn = json::parse(jsonString);

    auto configurationJson = jsonIn.find(className_);
    if(configurationJson == jsonIn.end())
    {
        throw std::runtime_error("The json I am trying to parse (" + jsonString + ") is not of class " + className_);
    }

    temperatures_.clearTemperatures();
    auto temperaturesJson = configurationJson.value().find(temperatures_.getClassName());

    for(auto& temperature: temperaturesJson.value()[0]) // There is only 1 object associated to className_
    {
        temperatures_.setTemperature(temperature["name"], temperature.get<BurninBoxTemperatures::Temperature>());
        std::cout << __PRETTY_FUNCTION__ << temperature["name"] << " --- " << temperatures_.getTemperature(temperature["name"]).temperature << std::endl;
    }
}
