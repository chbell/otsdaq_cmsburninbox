/*
 * BurninBoxTemperatures.cc
 *
 *  Created on: Sep 13, 2018
 *      Author: otsdaq
 */

#ifndef BEAGLEBONE
#include "BurninBox/BurninBoxUtils/BurninBoxTemperatures.h"
#else
#include "BurninBoxTemperatures.h"
#endif

#include <iostream>

using namespace ots;

//========================================================================================================================
BurninBoxTemperatures::BurninBoxTemperatures() : className_ ("BurninBoxTemperatures") {}

//========================================================================================================================
BurninBoxTemperatures::~BurninBoxTemperatures() {}

//========================================================================================================================
void BurninBoxTemperatures::clearTemperatures()
{
    temperatures_.clear();
}

//========================================================================================================================
void BurninBoxTemperatures::setTemperature(std::string name, double temperature, unsigned time, unsigned sequence)
{
    temperatures_[name] = Temperature(name, temperature, time, sequence);
}

//========================================================================================================================
void BurninBoxTemperatures::setTemperature(std::string name, const Temperature& temperature )
{
    temperatures_[name] = temperature;
}

//========================================================================================================================
BurninBoxTemperatures::Temperature BurninBoxTemperatures::getTemperature(std::string name) const
{
    auto temperature = temperatures_.find(name);
    if(temperature == temperatures_.end())
    {
        throw std::runtime_error("The temperature " + name + " is not in the list of temperatures!");
    }

    return temperature->second;
}

void BurninBoxTemperatures::printTemperatures()
{
    for(auto temperature: temperatures_)
        std::cout << __PRETTY_FUNCTION__ << temperature.first << " = " << temperature.second.temperature << std::endl; 
}
