#ifndef _ots_BurninBoxClient_h_
#define _ots_BurninBoxClient_h_

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BurninBox/BurninBoxUtils/TCPNetworkClient.h"
#include "otsdaq_cmsburninbox/BurninBox/BurninBoxUtils/BurninBoxConfiguration.h"
#else
#include "TCPNetworkClient.h"
#include "BurninBoxConfiguration.h"
#endif

#include <string>

namespace ots
{


class BurninBoxClient: public TCPNetworkClient
{
public:

	BurninBoxClient(const std::string& serverIP, int serverPort);
	virtual ~BurninBoxClient(void);

	void halt                (void);
	void pause               (void);
	void resume              (void);
	void start               (std::string runNumber, std::string& statusBuffer);
	void stop                (void);
	void configure           (void);
	//void setConfiguration    (float SetTemperature=20.0, float LowTolerance=1.0, float HighTolerance=1.0, float StopTemperature=22.0);
	void setConfiguration    (float SetTemperature=20.0, float StopTemperature=22.0);	
	std::string status       (void);
	std::string readConfig   (void);
	void interactiveCommand  (void); //settings frm terminal
	void getConfigFromInput  (bool color = true);
	void writeToFile         (std::string buffer);
	void printToScreen       (std::string buffer);
	std::string getRun       (void);
	bool isRunning           (void);
	bool isPaused            (void);
	void terminalInterface   (void); //supervised settings from terminal

protected:

	BurninBoxConfiguration theBurninBoxConfiguration_;
	void cinThread(std::string& statusBuffer);

};
}

#endif
