#ifndef _ots_BurninBoxServer_h_
#define _ots_BurninBoxServer_h_

#ifndef BEAGLEBONE
#include "otsdaq_cmsburninbox/BurninBox/BurninBoxUtils/TCPNetworkServer.h"
#include "otsdaq_cmsburninbox/BurninBox/BurninBoxUtils/BurninBoxConfiguration.h"
#include "otsdaq_cmsburninbox/BurninBox/BurninBoxUtils/BurninBoxController.h"
#else
#include "TCPNetworkServer.h"
#include "BurninBoxConfiguration.h"
#include "BurninBoxController.h"
#endif

#include <string>

namespace ots
{

class BurninBoxServer: public TCPNetworkServer
{
public:

	BurninBoxServer(BurninBoxController& burningBoxController, int serverPort, int bufferSize = 0x10000);
	virtual ~BurninBoxServer(void);

	std::string readMessage(const std::string& buffer);

protected:

	std::string getVariableValue(std::string variable, std::string buffer)
	{
		size_t begin = buffer.find(variable)+variable.size()+1;
		size_t end   = buffer.find(',', begin);
		if(end == std::string::npos)
			end = buffer.size()-1;
		return buffer.substr(begin,end-begin);
	}

	BurninBoxController&    theBurninBoxController_;
	BurninBoxConfiguration  theBurninBoxConfiguration_;
	std::string             currentRun_= "0";
	bool                    running_ = false;
	bool                    paused_=false;

};
}

#endif
