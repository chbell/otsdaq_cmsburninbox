#ifndef _ots_TCPNetworkSocket_h_
#define _ots_TCPNetworkSocket_h_

#include <netinet/in.h>
#include <mutex>
#include <string>

#define OTS_MAGIC 0x5F4F54534441515F /* _OTSDAQ_ */

namespace ots
{

class TCPNetworkSocket
{
public:
	TCPNetworkSocket(const std::string &senderHost, unsigned int senderPort, int receiveBufferSize = 0x10000);
	TCPNetworkSocket(unsigned int listenPort, int sendBufferSize = 0x10000);
	virtual ~TCPNetworkSocket(void);

	void connectSocket();

	int send(const uint8_t* data, size_t size);
	int send(const std::string& buffer);

	int receive(std::string& buffer, unsigned int timeoutSeconds = 1, unsigned int timeoutUSeconds = 0);
	int receive(uint8_t* buffer, unsigned int timeoutSeconds, unsigned int timeoutUSeconds);

protected:
	TCPNetworkSocket(void);

	//functions which were defined elsewhere (artdaq_mefextensions/mfextensions/Receivers/detail/*hh)
	int TCP_listen_fd(int port, int rcvbuf);
	int ResolveHost(char const* host_in, int dflt_port, sockaddr_in& sin);
	int TCPConnect(char const* host_in, int dflt_port, long flags = 0, int sndbufsiz = 0);

	std::string          host_;
	unsigned int         port_;
	int                  TCPSocketNumber_;
	int                  SendSocket_;
	bool                 isSender_;
	int                  bufferSize_;
	size_t               chunkSize_;

	mutable std::mutex   socketMutex_;

private:
	struct MagicPacket
	{
		uint64_t ots_magic;
		unsigned int dest_port;
	};
	MagicPacket makeMagicPacket(unsigned int port)
	{
		MagicPacket m;
		m.ots_magic = OTS_MAGIC;
		m.dest_port = port;
		return m;
	}
	bool checkMagicPacket(MagicPacket magic)
	{
		return magic.ots_magic == OTS_MAGIC && magic.dest_port == port_;
	}

};

}

#endif
