#ifndef _ots_THREAD_h_
#define _ots_THREAD_h_

#include <thread>
#include <atomic>
#include <mutex>

namespace ots
{

class Thread
{
public:
	Thread         (void);
	virtual ~Thread(void);

	void start(void);
	void stop (void);

protected:
	virtual void threadMain(void) = 0;

	std::atomic<bool> stopThread_;
	std::mutex        mutex_;

private:
	std::thread theThread_;

};
}
#endif 
