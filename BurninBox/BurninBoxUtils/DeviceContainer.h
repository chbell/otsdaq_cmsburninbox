#ifndef _ots_DeviceContainer_h_
#define _ots_DeviceContainer_h_

#include <string>
#include <vector>
#include <map>
#include "Device.h"

namespace ots
{

class DeviceContainer
{
public:
	DeviceContainer (void);
	~DeviceContainer(void);

	std::vector<std::string> getDeviceList (void);

	const Device& setDevice (const Device& device);
	const Device& setDevice (const ConfigurationAttributes& attributes);
	const Device& setDevice (std::string name, std::string connector, std::string displayName, std::string comment, std::string status);
	const Device& getDevice (std::string name);

private:
	bool checkAlreadySet(std::string name);
	//      <device name, pointer to sensor>
	std::map<std::string, Device> devices_;
};
}
#endif
