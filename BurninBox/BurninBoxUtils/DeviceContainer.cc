#include "DeviceContainer.h"

using namespace ots;
//===========================================================================
DeviceContainer::DeviceContainer()
{}

//===========================================================================
DeviceContainer::~DeviceContainer()
{
	devices_.clear();
}

//===========================================================================
std::vector<std::string> DeviceContainer::getDeviceList(void)
{
	auto list = std::vector<std::string>();
	for (auto it: devices_)
	{
		list.emplace_back(it.first);
	}
	return list;
}

//===========================================================================
const Device& DeviceContainer::setDevice(const Device& device)
{
	if(!checkAlreadySet(device.getName()))
		devices_[device.getName()] = device;
	return devices_[device.getName()];

}

//===========================================================================
const Device& DeviceContainer::setDevice(const ConfigurationAttributes& attributes)
{
	Device tmpDevice(attributes);
	return setDevice(tmpDevice);
}

//===========================================================================
const Device& DeviceContainer::setDevice(std::string name, std::string connector, std::string displayName, std::string comment, std::string status)
{
	if(!checkAlreadySet(name))
		devices_[name] = Device(name, connector, name, comment, status);
	return devices_[name];
}

//===========================================================================
const Device& DeviceContainer::getDevice(std::string name)
{
	auto device = devices_.find(name);
	if(device != devices_.end() && device->second.isOn())
	{
		return device->second;
	}
	else
	{
		throw std::runtime_error(std::string("The device: ") + name + std::string(" was not configured or it is set to off!"));
	}
}

//===========================================================================
bool DeviceContainer::checkAlreadySet(std::string name)
{
	if(devices_.find(name) != devices_.end())
	{
		throw std::runtime_error(std::string("The device: ") + name + std::string(" has been configured multiple times!"));
	}
	return false;
}
