/*
 * BurninBoxConfiguration.h
 *
 *  Created on: Sep 13, 2018
 *      Author: Emanuele Aucone, Lorenzo Uplegger
 */

#ifndef _ots_BurninBoxConfiguration_h_
#define _ots_BurninBoxConfiguration_h_

#ifndef BEAGLEBONE
#include "BurninBox/BurninBoxUtils/BurninBoxTemperatures.h"
#else
#include "BurninBoxTemperatures.h"
#endif

namespace ots
{
class BurninBoxConfiguration
{
  public:
    BurninBoxConfiguration();
    virtual ~BurninBoxConfiguration();
    const std::string& getClassName(void) {return className_;}
    const BurninBoxTemperatures& getTemperatures() const {return temperatures_;}
    void setTemperature(std::string name, double temperature, unsigned time, unsigned sequence=-1);

    std::string convertToJson(void);
    void        convertFromJson(std::string json);
    float       getTargetTemperature() const {return 3;}

  private:
    const std::string className_;
    BurninBoxTemperatures temperatures_;
};

} // namespace ots
#endif /* _ots_BurninBoxConfiguration_h_ */
