#ifndef _ots_ConfigurationReader_h_
#define _ots_ConfigurationReader_h_

#include "Device.h"
#include <string>
#include <vector>
#include <map>

namespace ots
{
	//This class is responsible for reading the configuration xml files and parse it producing a list of the configured devices
	class ConfigurationReader
	{
	public:
		ConfigurationReader(void);
		~ConfigurationReader(void) { ; }

		const ConfigurationAttributes &getTemperatureSensor(std::string name) const;
		const ConfigurationAttributes &getDewPointSensor(std::string name) const;
		const ConfigurationAttributes &getChiller(std::string name) const;
		const ConfigurationAttributes &getRelay(std::string name) const;
		const ConfigurationAttributes &getConnection(void) const;
		const ConfigurationAttributes &getEmail(void) const;

		void printConfiguration(void);

	private:
		const ConfigurationAttributes & getDevice(std::string category, std::string name) const;
		// example:
		// sensor_["TEMPERATURE"]  [0]             ["name"]   =    "RTD0";//Device*
		std::map<std::string, std::vector<ConfigurationAttributes>> devices_;
		ConfigurationAttributes connection_;
		ConfigurationAttributes email_;
	};
} // namespace ots
#endif
