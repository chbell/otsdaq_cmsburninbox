#include "ConfigurationReader.h"

#include <string>
#include <fstream>
#include <iostream>
#include <cstdio>
#include <sstream>
#include <vector>
#include <map>

using namespace ots;
//===========================================================================
ConfigurationReader::ConfigurationReader(void)
{
	std::string line;

	std::string email = "EMAIL";
	std::vector<std::string> emailAttributes;
	emailAttributes.push_back("Users");

	std::string controller = "CONTROLLER";
	std::vector<std::string> controllerAttributes;
	controllerAttributes.push_back("Protocol");
	controllerAttributes.push_back("Version");
	controllerAttributes.push_back("IPAddress");
	controllerAttributes.push_back("Port");

	std::vector<std::string> categoryList;
	categoryList.push_back("TEMPERATURE");
	categoryList.push_back("DEWPOINT");
	categoryList.push_back("RELAY");
	categoryList.push_back("CHILLER");

	std::vector<std::string> attributeList;
	attributeList.push_back("Name");
	attributeList.push_back("Status");
	attributeList.push_back("ControllerConnector");
	attributeList.push_back("DisplayName");
	attributeList.push_back("Comment");

	std::string category;
	bool inConfiguration = false;
	bool comment = false;
	int index = -1;
	unsigned int beginQuote;
	unsigned int endQuote;
	std::size_t beginSensor;
	std::string fileName;
	if(getenv("BURNINBOX_CONFIGURATION_FILE") != nullptr)
		fileName = getenv("BURNINBOX_CONFIGURATION_FILE");
	else
		fileName = "configuration/BurninBoxConfiguration.xml";

	std::ifstream sensorFile;
	sensorFile.open(fileName, std::fstream::in | std::fstream::out | std::fstream::app);
	if (sensorFile.is_open())
	{
		while (!sensorFile.eof())
		{
			getline(sensorFile, line);
			if (line == "")
				continue;
			if (line.find("<!--") != std::string::npos)
				comment = true;
			if (comment)
			{
				if (line.find("-->") != std::string::npos)
					comment = false;
				continue;
			}
			//std::cout << "LINE: " << line << std::endl;

			if (!inConfiguration)
			{
				if (line.find("<" + email + ">") != std::string::npos)
				{
					category = email;
					inConfiguration = true;
					index = -1;
				}
				else if (line.find("<" + controller + ">") != std::string::npos)
				{
					category = controller;
					inConfiguration = true;
					index = -1;
				}
				else
				{
					for (auto element : categoryList)
					{
						if (line.find("<" + element + ">") != std::string::npos)
						{
							category = element;
							inConfiguration = true;
							index = -1;
							break;
						}
					}
				}

				if (!inConfiguration)
				{
					std::cout << "UNRECOGNIZED CATEGORY IN LINE:" << line << std::endl;
					continue;
				}
			}
			else
			{
				if (line == ("</" + category + ">"))
				{
					inConfiguration = false;
					continue;
				}

				else if ((beginSensor = line.find('<')) != std::string::npos)
				{
					//std::cout << "Category: " << category << std::endl;
					if (category == email)
					{
						for (auto attribute: emailAttributes)
						{
							if (line.find(attribute) == std::string::npos)
							{
								throw std::runtime_error(std::string("ERROR: Can't find attribute ") + attribute + std::string(" in ") + fileName );
							}
							index = (int)line.find(attribute);
							index = index + attribute.size();
							index = line.find("=", index);
							beginQuote = line.find('\"', index);
							index = beginQuote;
							endQuote = line.find('\"', index + 1);

							email_[attribute] = line.substr(beginQuote + 1, endQuote - beginQuote - 1);
							//std::cout << "Attribute: " << attribute
							//		  << " Value: " << connection_[attribute] << std::endl;
						}
					}
					else if (category == controller)
					{
						for (auto attribute: controllerAttributes)
						{
							if (line.find(attribute) == std::string::npos)
							{
								throw std::runtime_error(std::string("ERROR: Can't find attribute ") + attribute + std::string(" in ") + fileName );
							}
							index = (int)line.find(attribute);
							index = index + attribute.size();
							index = line.find("=", index);
							beginQuote = line.find('\"', index);
							index = beginQuote;
							endQuote = line.find('\"', index + 1);

							connection_[attribute] = line.substr(beginQuote + 1, endQuote - beginQuote - 1);
							//std::cout << "Attribute: " << attribute
							//		  << " Value: " << connection_[attribute] << std::endl;
						}
					}
					else
					{
						devices_[category].push_back(std::map<std::string, std::string>());
						int vectorIndex = devices_[category].size() - 1;
						for (unsigned attributeN = 0; attributeN < attributeList.size(); attributeN++)
						{
							if (line.find(attributeList[attributeN]) == std::string::npos)
							{
								throw std::runtime_error(std::string("ERROR: Can't find attribute ") + attributeList[attributeN] + std::string(" in ") + fileName );
							}
							index = (int)line.find(attributeList[attributeN]);
							index = index + attributeList[attributeN].size();
							index = line.find("=", index);
							beginQuote = line.find('\"', index);
							index = beginQuote;
							endQuote = line.find('\"', index + 1);

							devices_[category][vectorIndex][attributeList[attributeN]] = line.substr(beginQuote + 1, endQuote - beginQuote - 1);
							//std::cout << "Attribute: " << attributeList[attributeN]
							//		  << " Value: " << devices_[category][vectorIndex][attributeList[attributeN]] << std::endl;
						}
					}

					if (line.find("/>", index) != std::string::npos)
						index = -1;
					//   if("do all checks for consistency")
				}
				else
					throw std::runtime_error("ERROR: Didn't find opening bra < !");
			}
		}
		sensorFile.close();
	}
	else
		throw std::runtime_error(std::string("ERROR: Can't open file ") + fileName + std::string("!"));

}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getDevice(std::string category, std::string name) const
{
	auto it = devices_.find(category);
	if (it == devices_.end())
	{
		throw std::runtime_error("ERROR: Can't find category " + category);
	}
	else
	{
		for(const ConfigurationAttributes& vIt: it->second)
		{
			if(vIt.find("Name") != vIt.end())
			{
				if(vIt.find("Name")->second.compare(name) == 0)
					return vIt;
			}
			else
			{
				throw std::runtime_error("ERROR: Can't find attribute Name!");
			}
		}
		throw std::runtime_error(std::string("WARNING: Didn't find sensor with name ") + name + std::string(" in category ") + category);
	}
}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getTemperatureSensor(std::string name) const
{
	return getDevice("TEMPERATURE", name);
}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getDewPointSensor(std::string name) const
{
	return getDevice("DEWPOINT", name);
}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getRelay(std::string name) const
{
	return getDevice("RELAY", name);
}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getChiller(std::string name) const
{
	return getDevice("CHILLER", name);
}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getConnection(void) const
{
	return connection_;
}

//===========================================================================
const ConfigurationAttributes& ConfigurationReader::getEmail(void) const
{
	return email_;
}

//===========================================================================
void ConfigurationReader::printConfiguration(void)
{
	for (auto device : devices_)
	{
		std::cout << "Device:" << device.first << " :" << std::endl;
		for (auto sensor : device.second)
		{
			for (auto attribute : sensor)
			{
				std::cout << attribute.first << ": " << attribute.second << ", ";
			}
			std::cout << std::endl;
		}
	}
}
