#ifndef BEAGLEBONE
#include "BurninBox/BurninBoxUtils/ConfigurationReader.h"
#include "BurninBox/BurninBoxUtils/DeviceContainer.h"
#include "BurninBox/BurninBoxUtils/BurninBoxController.h"
#include "BurninBox/BurninBoxUtils/BurninBoxConfiguration.h"
#else
#include "ConfigurationReader.h"
#include "DeviceContainer.h"
#include "BurninBoxController.h"
#endif

#include <iostream>
#include <thread>
#include <chrono>

using namespace ots;

int main(int argc, char **argv)
{
	std::cout << "Starting the BurninBox" << std::endl;
	//This class is responsible for reading the configuration xml files and parse it producing a list of the configured devices to the console
	ConfigurationReader theConfiguration;
	std::cout << "Configuration read" << std::endl;

	//This class knows how to control the BurninBox using the devices that have been configured
	BurninBoxController theBurninBoxController;

	std::cout << "Initialize the BurninBox" << std::endl;
	theBurninBoxController.checkConfiguration(theConfiguration);

	std::cout << "Running BurninBox!" << std::endl;
	theBurninBoxController.startAccept();

	while(1) //listen to port BurninBox_PORT
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
		//break;
	}

	return 0;
}
